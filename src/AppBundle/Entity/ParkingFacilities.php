<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * ParkingFacilities
 *
 * @ORM\Table(name="parking_facilities")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ParkingFacilitiesRepository")
 */
class ParkingFacilities
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="City", type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * One Parkingfacility have many Coordinates.
     * @ORM\OneToMany(targetEntity="Coordinates", mappedBy="parkingFacility", cascade={"persist"}, fetch="EAGER")
     */
    private $coordinates;

    /**
     * @ORM\Column(name="uuid", type="guid")
     */
    private $uuid;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ParkingFacilities
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->coordinates = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Add coordinate
     *
     * @param \AppBundle\Entity\Coordinates $coordinate
     *
     * @return ParkingFacilities
     */
    public function addCoordinate(\AppBundle\Entity\Coordinates $coordinate)
    {
        $this->coordinates[] = $coordinate;

        return $this;
    }

    /**
     * Remove coordinate
     *
     * @param \AppBundle\Entity\Coordinates $coordinate
     */
    public function removeCoordinate(\AppBundle\Entity\Coordinates $coordinate)
    {
        $this->coordinates->removeElement($coordinate);
    }

    /**
     * Get coordinates
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCoordinates()
    {
        return $this->coordinates;
    }

    /**
     * Set uuid
     *
     * @param guid $uuid
     *
     * @return ParkingFacilities
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get uuid
     *
     * @return guid
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return ParkingFacilities
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }
}
