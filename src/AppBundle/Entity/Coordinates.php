<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Coordinates
 *
 * @ORM\Table(name="coordinates")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CoordinatesRepository")
 */
class Coordinates
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Coordinates", type="string", length=255)
     */
    private $coordinates;


    /**
     * Many Coordinates have One ParkingFacility.
     * @ORM\ManyToOne(targetEntity="ParkingFacilities", inversedBy="coordinates", cascade={"persist"}, fetch="EAGER")
     */
    private $parkingFacility;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set coordinates
     *
     * @param string $coordinates
     *
     * @return Coordinates
     */
    public function setCoordinates($coordinates)
    {
        $this->coordinates = $coordinates;

        return $this;
    }

    /**
     * Get coordinates
     *
     * @return string
     */
    public function getCoordinates()
    {
        return $this->coordinates;
    }

    /**
     * Set parkingFacility
     *
     * @param \AppBundle\Entity\ParkingFacilities $parkingFacility
     *
     * @return Coordinates
     */
    public function setParkingFacility(\AppBundle\Entity\ParkingFacilities $parkingFacility = null)
    {
        $this->parkingFacility = $parkingFacility;

        return $this;
    }

    /**
     * Get parkingFacility
     *
     * @return \AppBundle\Entity\ParkingFacilities
     */
    public function getParkingFacility()
    {
        return $this->parkingFacility;
    }
}
