<?php

namespace AppBundle\Command;

use AppBundle\Entity\Coordinates;
use AppBundle\Entity\ParkingFacilities;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ImportParkingCoordinatesCommand extends Command
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:import-parking-coordinates')

            // the short description shown while running "php bin/console list"
            ->setDescription('Import parking coordinates')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command allows you to import parking facilities from the NPR')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $iteration = 1;
        $batchSize = 20;

        $this->container = $this->getApplication()->getKernel()->getContainer();
        $em = $this->container->get('doctrine')->getEntityManager();

        $parkingFacility = $em->getRepository('AppBundle:ParkingFacilities')->findAll(null,  ['id' => 'ASC']);

        foreach($parkingFacility as $parkingData) {

            $cords = null;

            $selectedFacility = $em->getRepository('AppBundle:ParkingFacilities')->findOneBy(['uuid' => $parkingData->getUuid()]);

            $io->title($parkingData->getName());

            $gpsdata = file_get_contents('https://npropendata.rdw.nl/parkingdata/v2/static/' . $parkingData->getUuid());
            $gpsdataObj = json_decode($gpsdata);


            if(isset($gpsdataObj->ParkingFacilityInformation->specifications[0]))
            {
                $locationsData = $gpsdataObj->ParkingFacilityInformation->specifications[0];

            } else {
                $locationsData = $gpsdataObj->parkingFacilityInformation->specifications[0];
            }

            // check if there is areaGeometry (to prevent empty crashes)
            if(isset($locationsData->areaGeometry)) {

                // Ignore everything else that is not poygon included multipolygon
                if ($locationsData->areaGeometry->type == 'Polygon' || !$locationsData->areaGeometry->type) {
                    // check if coordinates are available
                    if (isset($locationsData->areaGeometry->coordinates[0])) {

                        // GPS data
                        foreach ($locationsData->areaGeometry->coordinates[0] as $data) {

                            $cords = $data[1] . ',' . $data[0];

                            $io->note($cords);

                            $coordinates = new Coordinates();
                            $coordinates->setParkingFacility($selectedFacility);
                            $coordinates->setCoordinates($cords);

                            $em->merge($coordinates);

                            if (($iteration % $batchSize) === 0) {
                                $em->flush();
                                $em->clear(); // Detaches all objects from Doctrine!
                            }


                            $iteration++;
                        }
                    }
                }
            }


            // Get city location NEED TO CLEANUP!!

            if($cords != null) {

                $get_API = "https://maps.googleapis.com/maps/api/geocode/json?latlng=";
                $get_API .= $cords;
    //            $get_API .= round(4.746723314,2);

                $jsonfile = file_get_contents($get_API.'&key=AIzaSyD8A2rsN8tKitByveH8PHz2-5x4Awn7Njw&sensor=false');
                $jsonarray = json_decode($jsonfile);

                if (isset($jsonarray->results[1]->address_components[1]->long_name)) {
                    $selectedFacility->setCity($jsonarray->results[1]->address_components[1]->long_name);

                    $em->merge($selectedFacility);
                    $em->flush();

                    $io->note('Google location:' . $jsonarray->results[1]->address_components[1]->long_name);

                } else {

//                    $selectedFacility->setCity('Unknown');
//
//                    $em->merge($selectedFacility);
//                    $em->flush();

                    $io->note('Google location: UNKNOWN');
                }

//                $io->note('Google location:' . $jsonarray->results[1]->address_components[1]->long_name);


                // Locatie bevat geen positie
            } else {
//                $selectedFacility->setCity('Unknown');
//
//                $em->merge($selectedFacility);
//                $em->flush();

                $io->note('Google location: NO COORDINATES GIVEN'. $cords);
            }


        }

        $em->flush();
        $em->clear(); // Detaches all objects from Doctrine!
    }
}
