<?php

namespace AppBundle\Command;

use AppBundle\Entity\Coordinates;
use AppBundle\Entity\ParkingFacilities;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class LocationCommand extends Command
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:location')

            // the short description shown while running "php bin/console list"
            ->setDescription('Convert coordinates to location')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command allows you to iConvert coordinates to location')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $get_API = "http://maps.googleapis.com/maps/api/geocode/json?latlng=";
        $get_API .= round(52.637312761,2).",";
        $get_API .= round(4.746723314,2);

        $jsonfile = file_get_contents($get_API.'&sensor=false');
        $jsonarray = json_decode($jsonfile);

        if (isset($jsonarray->results[1]->address_components[1]->long_name)) {
            print($jsonarray->results[1]->address_components[1]->long_name);
        }
        else {
            print('Unknown');
        }
    }
}
