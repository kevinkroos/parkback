<?php

namespace AppBundle\Command;

use AppBundle\Entity\Coordinates;
use AppBundle\Entity\ParkingFacilities;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ImportParkingFacilitiesCommand extends Command
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:import-parking-facilities')

            // the short description shown while running "php bin/console list"
            ->setDescription('Import parking facilities')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command allows you to import parking facilities from the NPR')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $this->container = $this->getApplication()->getKernel()->getContainer();
        $em = $this->container->get('doctrine')->getEntityManager();

        $json = file_get_contents('https://npropendata.rdw.nl/parkingdata/v2/');
        $obj = json_decode($json)->ParkingFacilities;

        $io = new SymfonyStyle($input, $output);

        $iteration = 1;
        $batchSize = 20;

        foreach($obj as $data) {

            $io->title($data->name);

            $ParkingFacilitiy = new ParkingFacilities();
            $ParkingFacilitiy->setName($data->name);
            $ParkingFacilitiy->setUuid($data->uuid);

            $em->persist($ParkingFacilitiy);

            if (($iteration % $batchSize) === 0) {
                $em->flush();
                $em->clear(); // Detaches all objects from Doctrine!
            }

            $iteration++;
        }

        $em->flush(); //Persist objects that did not make up an entire batch
        $em->clear();
    }
}
