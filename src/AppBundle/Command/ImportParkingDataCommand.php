<?php

namespace AppBundle\Command;

use AppBundle\Entity\Coordinates;
use AppBundle\Entity\ParkingFacilities;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ImportParkingDataCommand extends Command
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:import-parking-data')

            // the short description shown while running "php bin/console list"
            ->setDescription('Import parking data')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command allows you to import parking data from the NPR')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $this->container = $this->getApplication()->getKernel()->getContainer();
        $em = $this->container->get('doctrine')->getEntityManager();

        $json = file_get_contents('https://npropendata.rdw.nl/parkingdata/v2/');
        $obj = json_decode($json)->ParkingFacilities;

        $io = new SymfonyStyle($input, $output);

        // Location data
        foreach($obj as $data) {

            $io->title($data->name);

            $ParkingFacilitiy = new ParkingFacilities();
            $ParkingFacilitiy->setName($data->name);

            $gpsdata = file_get_contents('https://npropendata.rdw.nl/parkingdata/v2/static/' . $data->uuid);
            $gpsdataObj = json_decode($gpsdata);

            $locationsData = $gpsdataObj->parkingFacilityInformation->specifications[0];

            // check if there is areaGeometry (to prevent empty crashes)
            if(isset($locationsData->areaGeometry)) {

                // Ignore everything else that is not poygon included multipolygon
                if($locationsData->areaGeometry->type == 'Polygon' || !$locationsData->areaGeometry->type) {


                    // check if coordinates are available
                    if(isset($locationsData->areaGeometry->coordinates[0])) {

                        // GPS data
                        foreach ($locationsData->areaGeometry->coordinates[0] as $data) {

                            $coordinates = new Coordinates();

                            $cords = $data[1] . ',' . $data[0];

                            $io->note($cords);

                            $coordinates->setCoordinates($cords);

                            $ParkingFacilitiy->addCoordinate($coordinates);

                            $em->persist($ParkingFacilitiy);
                            $em->flush();
                        }
                    }

                }
            }
        }
    }
}
